<?php

    require_once('main.php');
    $DBH = null;
    $DB = "WOK";
    $WEIGHT = array();
    function execDbStatement(PDOStatement $_op, $_args = array())
    {
        if (!$_op->execute($_args)) {
            $error_info = $_op->errorInfo();
            $error_info[2];
            throw new \Exception('db statement execute error: ' . $error_info[2]);
        }
        return $_op;
    }

    function parseDate($_d)
    {
        if (!$_d) return null;
        if (preg_match('/([0-9]{4})/', $_d, $m)) {
            return $m[0];
        }
        $_d = preg_replace('/-?\s?[A-Z]-?\s?*/', '', $_d);
        if (date_parse($_d)['year']) {
            return date_parse($_d)['year'];
        } else {
            $_d = preg_replace('/[A-Z]*/', '', $_d);
            if (preg_match('/[0-9]{2}/', $_d, $m)) {
                $d = intval($m[0]);
                if ($d < 20)
                    return "20" + $m[0];
                else
                    return "19" + $m[0];
            }
        }
        echo "could not parse $_d\n";
        return null;
    }

    function init_db()
    {
        global $DB, $DBH, $USER, $PASS;
        if ($DBH) return $DBH;
        try {
            $DBH = new PDO("mysql:dbname={$DB};host=localhost", $USER, $PASS);
        } catch (PDOException $e) {
            die("ERROR: {$e->getMessage()}");
        }
        return $DBH;
    }


    function insertDbEntries($_idx_key = "REFID")
    {
        global $WEIGHT;
        //json files
        $entries = json_decode(file_get_contents('entries.json'), true);
//        $result_entries = json_decode(file_get_contents('result_entries.json'), true);
        echo sizeof($entries) . " entries\n";

        //-----------------------------db op handler-----------------------//
        $dbh           = init_db();
        $insert_entry  = $dbh->prepare('INSERT INTO entry (UT,title,DOI,REFID,abstract,published)VALUES (?,?,?,?,?,?);');
        $insert_result = $dbh->prepare('INSERT INTO result_entry(REFID)VALUE(?)');
        $insert_attr   = array(
            "author"         => $dbh->prepare("INSERT INTO author({$_idx_key},author)VALUES(?,?);"),
            "research_area"  => $dbh->prepare("INSERT INTO research_area({$_idx_key},research_area)VALUES(?,?);"),
            "WOS_category"   => $dbh->prepare("INSERT INTO WOS_category({$_idx_key},WOS_category)VALUES(?,?);"),
            "source"         => $dbh->prepare("INSERT INTO source({$_idx_key},source)VALUES(?,?);"),
            "author_keyword" => $dbh->prepare("INSERT INTO author_keyword({$_idx_key},author_keyword)VALUES(?,?);"),
            "keywords_plus"  => $dbh->prepare("INSERT INTO keywords_plus({$_idx_key},keywords_plus)VALUES(?,?);"),
            "reference"      => $dbh->prepare("INSERT INTO reference({$_idx_key}_I,{$_idx_key}_II)VALUES(?,?)"),
            "citation"       => $dbh->prepare("INSERT INTO citation({$_idx_key}_I,{$_idx_key}_II)VALUES(?,?)"),

        );
        //------------------------truncate tables------------------------//
        $dbh->exec('TRUNCATE TABLE entry;');
        $dbh->exec('TRUNCATE TABLE result_entry;');
        foreach ($insert_attr as $key => $value) {
            $dbh->exec('TRUNCATE TABLE ' . $key . ';');
        }

        //----------------prepare insert transactions--------------------//
        $dbh->beginTransaction();

        foreach ($entries as $entry) {
            execDbStatement($insert_entry, array($entry['UT'], $entry['title'], $entry['DOI'], $entry['REFID'], $entry['abstract'], parseDate($entry['published'])));
            foreach ($insert_attr as $key => $op) {
                if (isset($entry[$key]) && !empty($entry[$key])) {
                    if (is_array($entry[$key])) {
                        foreach ($entry[$key] as $value) {
                            execDbStatement($op, array($entry[$_idx_key], $value));
                        }
                    } else {
                        execDbStatement($op, array($entry[$_idx_key], $entry[$key]));
                    }
                }
            }
        }
        $dbh->commit();
        unset($dbh);
    }


    function insertDbAuthors($_author_array = null)
    {
        $dbh = init_db();
        $dbh->exec('TRUNCATE TABLE author_detail;');
        $dbh->beginTransaction();
        $AUTHORS              = $_author_array ? $_author_array : json_decode(file_get_contents('authors.json'), true);
        $authors_in_graph     = getAuthorsInGraph();
        $author_cnts          = array(
            json_decode(file_get_contents('authors_unrefined.json'), true),
            json_decode(file_get_contents('authors_refined.json'), true),
            json_decode(file_get_contents('authors_refined_alt.json'), true),
        );
        $insert_author_detail = $dbh->prepare('INSERT INTO author_detail(author,short_name,address,entry_cnt,citation_cnt,entry_cnt_refined,citation_cnt_refined,entry_cnt_refined_alt,citation_cnt_refined_alt)VALUE(?,?,?,?,?,?,?,?,?)');
        foreach ((array)$AUTHORS as $key => $a) {
            if (array_search($a['name'], $authors_in_graph) === false)
                continue;
            $author = (array)$a;
            $a_I    = $author_cnts[0][$key];
            $a_II   = $author_cnts[1][$key];
            $a_III  = $author_cnts[2][$key];
            echo $author['name'] . "\n";
            $insert_author_detail->execute(array($author['name'], $author['short_name'], $author['address'],
                                               $a_I['entry_cnt'], $a_I['citation_cnt'],
                                               $a_II['entry_cnt'], $a_II['citation_cnt'],
                                               $a_III['entry_cnt'], $a_III['citation_cnt']));

        }
        $dbh->commit();
        unset($dbh);
    }

    function insertCountComparison()
    {
        $authors_in_graph = getAuthorsInGraph();
        $author_cnts      = array(
            json_decode(file_get_contents('authors_unrefined.json'), true),
            json_decode(file_get_contents('authors_refined.json'), true),
            json_decode(file_get_contents('authors_refined_alt.json'), true),
        );
        $dbh              = init_db();
        $dbh->exec('TRUNCATE TABLE author_count_comparison;');
        $dbh->beginTransaction();
        $insert_author_detail = $dbh->prepare('INSERT INTO author_count_comparison(author,entry_cnt_I,entry_cnt_II,entry_cnt_III,citation_cnt_I,citation_cnt_II,citation_cnt_III)VALUE(?,?,?,?,?,?,?);');
        foreach ($author_cnts[0] as $key => $a) {
            if (array_search($a['name'], $authors_in_graph) === false)
                continue;
            $a_I   = $a;
            $a_II  = $author_cnts[1][$key];
            $a_III = $author_cnts[2][$key];
            echo $a['name'] . "\n";
            $insert_author_detail->execute(array($a_I['name'],
                                               $a_I['entry_cnt'], $a_I['citation_cnt'],
                                               $a_II['entry_cnt'], $a_II['citation_cnt'],
                                               $a_III['entry_cnt'], $a_III['citation_cnt']));

        }
        $dbh->commit();
        unset($dbh);
    }


    function getAuthorResearchArea($_authorName)
    {
        $dbh    = init_db();
        $result = $dbh->query('SELECT DISTINCT r.research_area FROM entry e, author a, research_area r WHERE a.UT = e.UT AND r.UT = a.UT AND a.author=' . "'" . $_authorName . "'");
        if (!$result) return array();
        $result = $result->fetchAll();
        return array_column($result, 'research_area');
    }

    function getAuthorsInGraph($_suffix = 'cocitaion')
    {
        $dbh    = init_db();
        $result = $dbh->query("SELECT DISTINCT author_I FROM author_{$_suffix}");
        return array_column($result->fetchAll(), 'author_I');
    }


    function refMatrixOps()
    {
        global $DB;
        $DB = 'WOK_II';
        insertDbEntries();
        echo "entry...";
        getEntryCoreference();
        foreach (array('author', 'source','research_area') as $attr) {
            echo "$attr...";
            writeCoreference($attr);
        }
    }

    if (basename($argv[0]) == basename(__FILE__)) {
        refMatrixOps();
    }


?>
