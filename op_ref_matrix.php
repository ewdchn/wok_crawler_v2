<?php
    require_once("graph.php");
    global $DB;
    $DB = 'WOK_II';
    writeEntriesToDb();
    echo "entry...";
    writeEntryCoreferenceGraph();
    foreach (array('author', 'source', 'research_area') as $attr) {
        echo "$attr...";
        writeCoreferenceGraph($attr);
    }
    system('python graphGenerator.py');
    system('python csvGenerator.py');
?>