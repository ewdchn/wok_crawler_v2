<?php
    require_once('common/CitationGraph.php');
    require_once('common/Graph.php');
    require_once('common/Helper.php');
    require_once('Crawler.php');
    require_once('Parser.php');
    require_once('Entry.php');
    require_once('db_op.php');

    const SITEURL = "www.webofknowledge.com/";
    $IDX = "REFID";
    $ATTR = array("author","source","research_area");
    $LOGFILE_NO = 0;
    $TOTAL_CITATION_CNT = 0;
    //=======================================================================================================================//
    //===========================================UTILITY FUNCTIONS===========================================================//
    function formatResearchAreaString(&$_str) { $_str = 'ResearchArea_' . strtoupper(str_replace('  ', ' ', preg_replace('/[,&\-_.]/', '', $_str))); }

    function catchError($errno, $errstr, $errfile, $errline, array $errcontext) { throw new ErrorException($errstr, 0, $errno, $errfile, $errline); }

    /**
     * get doc count/ cite count for all authors
     *
     * @param $_author_list
     * @param int $_refine
     */
    function authorRefSearch($_author_list, $_refine = 1)
    {
        $authors_in_graph = getAuthorsInGraph();
        $crawler          = new \Crawler\Crawler();
        array_map(function (Author $_author) use ($crawler, $authors_in_graph, $_refine) {
                if (array_search($_author->getFullName(), $authors_in_graph) === false) {
                    unset($_author);
                    return;
                }
                echo $_author->getFullName() . " ";
                $crawler = new \Crawler\Crawler();
                $try_cnt = 0;
                while (true) {
                    try {
                        //author search
                        $r = $crawler->authorSearch($_author->getFullName());
                        //==================refine search=======================//
                        if ($_refine === false) {
                            $cit_report_lnk = \Parser\parseAuthorSearchPage($r);
                        } else if ($_refine == 1) {
                            $sess_attr      = \Parser\parseRefSearchPage($r);
                            $cit_report_lnk = \Parser\parseAuthorSearchPage($crawler->refineSearch($sess_attr['qid'], $sess_attr['SID']));
                        } else if ($_refine == 2) {
                            $sess_attr            = \Parser\parseRefSearchPage($r);
                            $author_research_area = getAuthorResearchArea($_author->getFullName());
                            array_walk($author_research_area, 'formatResearchAreaString');
                            echo implode(', ', $author_research_area) . "\n";
                            if (!empty($author_research_area)) {
                                $cit_report_lnk = \Parser\parseAuthorSearchPage($crawler->refineSearchAlt($sess_attr['qid'], $sess_attr['SID'], $author_research_area));
                            } else {
                                $cit_report_lnk = \Parser\parseAuthorSearchPage($r);
                            }
                        }
                        //============================get attrs===========================//
                        if ($cit_report_lnk) {
                            echo "report" . "\n";
                            $cit_report_lnk = 'apps.webofknowledge.com' . $cit_report_lnk;
                            list($entry_cnt, $cit_count) = \Parser\parseCitationReport($crawler->getCitationReport($cit_report_lnk));
                            echo $entry_cnt . ' ' . $cit_count . "\n";
                            break;
                        } else {
                            if (isset($entry_cnt) && isset($cit_count)) {
                                break;
                            }
                            list($entry_cnt, $cit_count) = array(0, 0);
                            if ($cit_report_lnk === false)
                                break;
                            echo "new sess\n";
                            $crawler = new \Crawler\Crawler();
                            continue;
                        }
                    } catch (Exception $e) {
                        fwrite(STDOUT, '!');
                        if ($try_cnt++ > 3) {
                            $entry_cnt = $cit_count = 0;
                            break;
                        }
                        continue;
                    }
                }
                $_author->setEntryCnt($entry_cnt);
                $_author->setCitationCnt($cit_count);
            },
            $_author_list);
    }

    /**
     * The result returned by Web of Science about author's document count/ citaiton count is fuzzy
     *
     * So there are 2 ways the refine the result
     *
     */
    function getAuthorCounts()
    {
        Author::constrctFromJson(file_get_contents('authors.json'));
        authorRefSearch(Author::$author_list);
        file_put_contents('authors_refined.json', json_encode(Author::$author_list));
        //====================//
        Author::constrctFromJson(file_get_contents('authors.json'));
        authorRefSearch(Author::$author_list, false);
        file_put_contents('authors_unrefined.json', json_encode(Author::$author_list));
        //=====================//
        Author::constrctFromJson(file_get_contents('authors.json'));
        authorRefSearch(Author::$author_list, 2);
        file_put_contents('authors_refined_alt.json', json_encode(Author::$author_list));
    }


    function getCitation($_REFID)
    {
        global $TOTAL_CITATION_CNT, $DEBUG;
        $wos_array = array();
        $crawler   = new \Crawler\Crawler();

        //citation Search
        $try_cnt = 0;
        while (true) {
            try {
                $sess_attr = \Parser\parseRefSearchPage($crawler->citationSearch($_REFID));
                break;
            } catch (Exception $e) {
                fwrite(STDOUT, '!');
                if ($try_cnt++ > 3) {
                    return array();
                }
                continue;
            }
        }
        $TOTAL_CITATION_CNT += $sess_attr['result_cnt'];
        for ($i = 1; $i <= $sess_attr['result_cnt']; $i++) {
            $try_cnt = 0;
            while (true) {
                try {
                    $entry_attr = \Parser\parseEntryPage($crawler->grabEntryPageRefMode($sess_attr['qid'], $sess_attr['SID'], $i));
                    if (!$entry_attr['title']) {
                        throw new \Exception;
                    } else if ($entry_attr['UT']) {
                        $wos_array[] = $entry_attr['UT'];
                    }
                    break;
                } catch (Exception $e) {
                    fwrite(STDOUT, '!');
                    echo $e->getTraceAsString();
                    if ($try_cnt++ > 5) {
//                        writeLog($tmp->html);
                        exit(0);
                    }
                    unset($tmp);
                    continue;
                }
            }
        }
        unset($crawler);
        return $wos_array;
    }

    function getReference($_UT)
    {
        $crawler = new \Crawler\Crawler();
        echo "\n" . $_UT . "," . "\n";
        $refid_list = array();
        $response   = $crawler->refSearch($_UT);
        while (true) {
            try {
                $tmp = \Parser\parseRefPage($response);
                echo "///";
            } catch (\Exception $e) {
                echo $_UT . "failed\n";
                break;
            }
            foreach ($tmp['blocks'] as $t) {
                try {
                    if (isset($t['link'])) {
                        $entry        = Entry::findEntry(\Parser\parseEntryPage($crawler->getPageWithURL($t['link'])));
                        $refid_list[] = $entry->attr['REFID'];
                    } else {
                        $entry        = Entry::findEntry($t);
                        $refid_list[] = $t['REFID'];
                    }
                } catch (\Exception $e) {
                    echo "Failure at: " . $_UT . "\n";
                    var_dump($t);
                }
            }
            if ($tmp['nextpage']) {
//                var_dump($tmp['nextpage']);
                $response = $crawler->getPageWithURL($tmp['nextpage']);
            } else {
                break;
            }
        }
        return $refid_list;
    }


    /**
     * Crawling logic
     *
     * 1. Sent a POST query to search, get the qid and SID for this search query
     *
     * 2. Result entries are accessble by giving a doc no in GET request
     *
     *    e.g. doc=1 directs to the first result of search
     * 
     */

    function referenceSearch()
    {
        $reconnect_cnt = 0;
        $crawler       = new \Crawler\Crawler();
        $sess_attr     = \Parser\parseTopicSearchPage($crawler->topicSearch());
        echo "results:" . $sess_attr['result_cnt'];

        $result_entry = array();
        //============get the L0 entries, parse them, get the shared record search lnk==============//
        for ($i = 1; $i <= $sess_attr['result_cnt']; $i++) {
            echo '.';
            $try_cnt = 0;
            while (true) {
                try {
                    $entry_attr = \Parser\parseEntryPage($crawler->grabEntryPage($sess_attr['qid'], $sess_attr['SID'], $i));
                    foreach ($entry_attr['author_detail'] as $author) {
                        $a                      = Author::getAuthor($author['name'], $author['short_name'], $author['address']);
                        $entry_attr['author'][] = $a->getFullName();
                    }
                    $tmp            = Entry::findEntry($entry_attr);
                    $result_entry[] = $entry_attr['REFID'];
                    $tmp->setReference(getReference($entry_attr['UT']));
                    break;
                } catch (Exception $e) {
                    fwrite(STDOUT, "\n" . '!');
                    echo $e->getTraceAsString();
                    if ($try_cnt++ >= 5) {
                        log_html($tmp->html);
                        if ($reconnect_cnt++ > 5) {
                            die("error occured stopping");
                        } else {
                            //get a new session
                            $crawler   = new \Crawler\Crawler();
                            $sess_attr = \Parser\parseTopicSearchPage($crawler->topicSearch());
                        }
                    }
                    unset($tmp);
                    continue;
                }
            }
        }
        return $result_entry;
    }


    function citationSearch()
    {
        $reconnect_cnt = 0;
        $crawler       = new \Crawler\Crawler();
        $sess_attr     = \Parser\parseTopicSearchPage($crawler->topicSearch());
        echo "results:" . $sess_attr['result_cnt'];
        //make it sort
        getPage(array(
                            CURLOPT_URL => 'http://apps.webofknowledge.com/summary.do?product=UA&parentProduct=UA&search_mode=GeneralSearch&qid=' . $sess_attr['qid'] . '&page=1&action=sort&sortBy=TC.D;PY.D;AU.A;SO.A;VL.D;PG.A'
                        ),
                        $crawler->cookie_storage);

        for ($i = 1; $i <= $sess_attr['result_cnt']; $i++) {
            echo '.';
            $try_cnt = 0;
            while (true) {
                try {
                    $entry_attr = \Parser\parseEntryPage($crawler->grabEntryPage($sess_attr['qid'], $sess_attr['SID'], $i));
                    foreach ($entry_attr['author_detail'] as $author) {
                        $a                      = Author::getAuthor($author['name'], $author['short_name'], $author['address']);
                        $entry_attr['author'][] = $a->getFullName();
                    }
                    $tmp                   = new Entry($entry_attr);
                    $tmp->attr['citation'] = $entry_attr['has_citation'] ? getCitation($tmp->attr['REFID']) : array();
                    break;
                } catch (Exception $e) {
                    fwrite(STDOUT, "\n" . '!');
                    echo $e->getTraceAsString();
                    if ($try_cnt++ >= 5) {
                        log_html($tmp->html);
                        if ($reconnect_cnt++ > 5) {
                            die("error occured stopping");
                        } else {
                            //get a new session
                            $crawler   = new \Crawler\Crawler();
                            $sess_attr = \Parser\parseTopicSearchPage($crawler->topicSearch());
                        }
                    }
                    unset($tmp);
                    continue;
                }
            }
        }
    }

    function crawlCitation()
    {
        global $TOTAL_CITATION_CNT;
        citationSearch();
        echo "\ntotal citation" . $TOTAL_CITATION_CNT;
        file_put_contents('entries.json', json_encode(Entry::$entry_list));
        file_put_contents('authors.json', json_encode(\Author::$author_list));
    }

    function crawlReference()
    {
        $result_entries = referenceSearch();
        file_put_contents('result_entries.json', json_encode($result_entries));
        file_put_contents('entries.json', json_encode(Entry::$entry_list));
        file_put_contents('authors.json', json_encode(Author::$author_list));
        insertDbEntries();
        getEntryCoreference();

    }

    if (basename($argv[0]) !== basename(__FILE__)) {
        return;
    } else {
//        crawlCitation();
        crawlReference();
    }

?>
