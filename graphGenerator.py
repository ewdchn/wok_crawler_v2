# -*- coding: utf-8 -*-
import sys
import os
import MySQLdb as mdb
import passwd
prefix = ['author','entry','research_area','source']
if len(sys.argv)<2:
    suffix = 'coreference'
else:
    suffix = sys.argv[1]
con = mdb.connect("localhost",passwd.USER,passwd.PASSWD,"WOK_II")
for p in prefix:
    table = p+'_'+suffix
    if p == 'entry':
        p = 'REFID'
    print table
    cur = con.cursor()
    cur.execute('SELECT COUNT(*) FROM (SELECT {0}_I FROM {1} UNION SELECT {0}_II FROM {1})x'.format(p,table))
    node_count = cur.fetchone()[0]
    cur.execute('SELECT * FROM {0}'.format(table))
    rows = cur.fetchall()
    output = []
    output.append('dl n = {0} format = edgelist1'.format(node_count))
    output.append('labels embedded')
    output.append('data:')
    for row in rows:
            output.append('"{0}" "{1}" {2}'.format(row[0],row[1],row[2]))
    output_file = file('{0}.dl'.format(table),'w+')
    output_file.writelines(map(lambda x:x+"\n",output))
