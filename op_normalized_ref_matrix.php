<?php

    require_once('graph.php');
    $DB = 'WOK_II';
    writeEntriesToDb();
    writeEntryCoreferenceGraph(true);
    foreach (array('author', 'source', 'research_area') as $attr) {
        writeCoreferenceGraph($attr,true);
    }
    system('python graphGenerator.py');
    system('python csvGenerator.py');
?>