<?php
    namespace Parser;
    require_once('simple_html_dom.php');
    require_once('common/Helper.php');
    require_once('Author.php');
    use simple_html_dom\simple_html_dom;

    /**
     * @param simple_html_dom_node $_node
     * @return array
     * on success return array of text nodes
     */
    function getTextBlocks($_node)
    {
        $ans = array();
        foreach ($_node->find('text') as $text) {
            $ans[] = $text->plaintext;
        }
        return $ans;
    }

    /**
     * @param $_text_block
     * @param $_name short name of author
     * @return null|string
     */
    function getAuthorFullName($_text_block, $_name)
    {
        foreach ($_text_block as $i => $blk) {
            if (strpos($blk, $_name) !== false) {
                if (strpos($_text_block[$i + 1], '(') !== false) {
                    return trim($_text_block[$i + 1], '(); ');
                } else {
                    return null;
                }
            }
        }
        return null;
    }

    /**
     * @param $_header_str
     * @return array
     */
    function split_http_headers($_header_str)
    {
        $header_attr  = array();
        $header_lines = explode("\r\n", $_header_str); //STATUS CODE
        array_shift($header_lines);
        foreach ($header_lines as $line) {
            $tmp_arr                  = explode(": ", $line, 2);
            $header_attr[$tmp_arr[0]] = $tmp_arr[1];
        }
        return $header_attr;
    }

    /**
     * @param simple_html_dom_node $_node
     * @return array
     */
    function parseRefBlock($_node)
    {
        $ans = array(
            'REFID'  => null,
            'title'  => null,
            'source' => null,
            'author' => array(),
        );
        //=================REFID=================//
        parse_str(parse_url($_node->find('a[title="View all of the articles that cite this one"]', 0)->href, PHP_URL_QUERY), $lnk_attr);
        $ans['REFID'] = $lnk_attr['REFID'];
        //=================attributes==============//
        $fcr = $_node->find('.fcr-normal-data') ? $_node->find('.fcr-normal-data', 0) : $_node;
        foreach ($fcr->find('div') as $div) {
            if (!$div->find('.label')) continue;
            $label = $div->find('.label', 0)->innertext;
//            echo $label;
            if (strpos($label, 'Title') !== false) {
                $ans['title'] = array_pop(getTextBlocks($div));
            } else if (strpos($label, 'By') === 0) {
                $ans['author'] = explode(';', array_pop(getTextBlocks($div)));
                if ($node = $div->next_sibling()) {
                    $ans['source'] = trim(array_shift(getTextBlocks($node)), html_entity_decode('&nbsp;'));
                }
                break;
            }
        }

        //title
        if ($title_node = $_node->find('.reference-title', 0)) {
            $ans['title'] = $title_node->find('value', 0)->innertext;
        }
//        var_dump($ans);
        return $ans;
    }

    /**
     * @param $response
     * @return null
     */
    function parseAuthorSearchPage($response)
    {
        if (is_array($response)) {
            $header_cnt = $response[2];
            $headers    = seperateHeader($response[0], $response[2]);
            $html       = $headers[$header_cnt];
        } else {
            $html = $response;
        }
        $dom                 = new simple_html_dom($html);
        $citation_report_lnk = $dom->find('a[title="View Citation Report"]', 0) ? $dom->find('a[title="View Citation Report"]', 0)->href : null;
        if (!$citation_report_lnk && $dom->find('.CitCountError'))
            return false;
        return $citation_report_lnk;
    }


    function parseCitationReport($response)
    {
        if (is_array(($response))) {
            $header_cnt = $response[2];
            $headers    = seperateHeader($response[0], $response[2]);
            $html       = $headers[$header_cnt];
            $dom        = new simple_html_dom($html);
        } else {
            $html = $response;
        }
        $dom = new simple_html_dom($html);
        if ($dom->find('table.Cit_Report_Break_Table')) {
            $table = $dom->find('table.Cit_Report_Break_Table', 0);
        } else if ($dom->find('.stats table')) {
            $table = $dom->find('.stats table', 0);
        } else {
            echo "table not found\n";
            throw new \Exception;
        }
        foreach ($table->find('tr') as $row) {
            $key   = $row->find('td.graphData', 0) ? $row->find('td.graphData', 0)->plaintext : null;
            $value = $row->find('td.graphData_data', 0) ? $row->find('td.graphData_data', 0)->plaintext : null;
            if ($key && $value) {
                if (strpos(trim($key), 'Results found') !== false) {
                    $result_cnt = intval(trim($value));
                } else if (strpos(trim($key), 'Sum of the Times Cited') !== false) {
                    $cit_cnt = intval(trim($value));
                }
            }
        }
        if (is_null($result_cnt) || is_null($cit_cnt)) {
            echo "count not found\n";
            throw new \Exception;
        }
        return array($result_cnt, $cit_cnt);
    }

    /**
     * @param $response
     * @return array[result_cnt,SID,qid]
     * @throws \Exception
     *
     */
    function parseTopicSearchPage($response)
    {
        $header_cnt  = $response[2];
        $headers     = seperateHeader($response[0], $response[2]);
        $header_attr = split_http_headers($headers[0]);
        if (!$header_attr) {
            throw new \Exception;
        } else {
            parse_str(parse_url($header_attr['Location'], PHP_URL_QUERY), $url_attr);
        }

        //get SID
        $SID  = $url_attr["SID"];
        $html = $headers[$header_cnt];
        //get resultCnt
        $dom        = new simple_html_dom($html);
        $result_cnt = intval($dom->find('#trueFinalResultCount', 0)->innertext);
        //get qid
        $qid_lnk = html_entity_decode($dom->find('a.smallV110', 0)->href);
        parse_str(parse_url($qid_lnk, PHP_URL_QUERY), $qid_lnk_attr);
        $qid = $qid_lnk_attr['qid'];
        $dom->clear();
        return array('result_cnt' => $result_cnt, 'SID' => $SID, 'qid' => $qid);
    }


    function parseRefPage($_response)
    {
        $dom = new simple_html_dom($_response);
        $ans = array('blocks' => array(), 'nextpage' => false);
        if (!$dom->find('.search-results-item')) {
            log_html($_response);
            throw new \Exception;
        }

        foreach ($dom->find('.search-results-item') as $block) {
            if ($a = $block->find('a.smallV110', 0)) {
                $ans['blocks'][] = array('link' => $a->href);
            } else {
                $ans['blocks'][] = parseRefBlock($block);
            }
        }
        if ($dom->find('.paginationNextDisabled')) {
            $ans['nextpage'] = false;
        } else if ($dom->find('.paginationNext')) {
            $ans['nextpage'] = $dom->find('.paginationNext', 0)->href;
        }
        unset($dom);
        return $ans;
    }

    /**
     * @param $response
     * @return array['result_cnt','SID','qid']
     *
     */
    function parseRefSearchPage($response)
    {
        $SID = null;

        if (is_array($response)) {
            $header_cnt = $response[2];
            $headers    = seperateHeader($response[0], $response[2]);
            $html       = $headers[$header_cnt];
        } else {
            $html = $response;
        }

        //get resultCnt
        $dom        = new simple_html_dom($html);
        $result_cnt = $dom->find('#trueFinalResultCount', 0) ? intval($dom->find('#trueFinalResultCount', 0)->innertext) : false;
        //get qid
        if (!$dom->find('a.smallV110') || !$result_cnt) {
            return array('result_cnt' => 0, 'SID' => $SID, 'qid' => 0);
        }
        $qid_lnk = html_entity_decode($dom->find('a.smallV110', 0)->href);
        parse_str(parse_url($qid_lnk, PHP_URL_QUERY), $qid_lnk_attr);
        $qid = $qid_lnk_attr['qid'];
        $SID = isset($qid_lnk_attr['SID']) ? $qid_lnk_attr['SID'] : null;
        $dom->clear();
        return array('result_cnt' => $result_cnt, 'SID' => $SID, 'qid' => $qid);
    }

    /**
     * @param $response
     * @return array
     */
    function parseEntryPage($response)
    {

        $ans = array(
            //
            "UT"             => null,
            "title"          => null,
            "DOI"            => null,
            "abstract"       => null,
            //
            "source"         => null,
            "WOS_category"   => null,
            "research_area"  => null,
            "REFID"          => null,
            //
            "author"         => array(),
            "author_keyword" => array(),
            "keywords_plus"  => array(),
            "citation"       => array(),
            //
            'author_detail'  => array(),
            'has_citation'   => false,
            'reference'      => array(),
        );
        $dom = new simple_html_dom(html_entity_decode($response));
        if (!$dom->find('.title value', 0)) {
            file_put_contents('parser_error.htm', $response);
            throw new \Exception;
        }
        //============================TITLE=============================================//
        $ans['title'] = str_replace('  ', ' ', $dom->find('.title value', 0)->plaintext);

        //============================REFID==============================================//
        if ($cit_lnk = $dom->find('a[title="View all of the articles that cite this one"]', 0)) {
            $cnt_lnk_attr = array();
            parse_str(parse_url($cit_lnk->href, PHP_URL_QUERY), $cnt_lnk_attr);
            $ans['REFID']        = $cnt_lnk_attr['REFID'];
            $ans['has_citation'] = true;
        } else {
            preg_match('/REFID=([0-9]+)&/', $response, $m);
            $ans['REFID'] = $m[1];
        }

        //============================ABSTRACT=============================================//
        foreach ($dom->find('.block-record-info') as $block) {
            if ($title = $block->find('.title3', 0)) {
                if (strpos($title->plaintext, "Abstract") !== false) {
                    $ans['abstract'] = $title->next_sibling()->plaintext;
                    break;
                }
            }
        }
        //============================SOURCE==============================================//
        $src_node = $dom->find('p.sourceTitle value', 0);
        if ($src_node) {
            $ans['source'] = $src_node->innertext;
        }

        //================================ADDRESS=========================================//
        $address_list = array();
        foreach ($dom->find('a[id^="address"]') as $a) {
            $addr                           = preg_split('/\s*[\[\]]\s*/', $a->plaintext, 2, PREG_SPLIT_NO_EMPTY);
            $address_list[intval($addr[0])] = $addr[1];
        }
        //================================ATTRIBUTES=========================================//
        foreach ($dom->find('p.FR_field') as $FR_node) {

            $key   = @$FR_node->find('span.FR_label', 0)->innertext;
            $value = @$FR_node->find('value', 0)->innertext;
            $value = $value ? $value : trim(end(explode('</span>', @$FR_node->innertext)));

            //================================KEYWORDS=========================================//
            if (strpos($key, 'Author Keywords:') !== false) {
                foreach ($FR_node->find('a') as $a) {
                    parse_str(parse_url($a->href, PHP_URL_QUERY), $url_attr);
                    $ans['author_keyword'][] = $url_attr['value'];
                }
            } //===============================PUBLISHED===========================================//
            else if (strpos($key, 'Published:') !== false) {
                $ans['published'] = $value;
            } //==================================KEYWORDS PLUS================================//
            else if (strpos($key, 'KeyWords Plus:') !== false) {
                foreach ($FR_node->find('a') as $a) {
                    parse_str(parse_url($a->href, PHP_URL_QUERY), $url_attr);
                    $ans['keywords_plus'][] = $url_attr['value'];
                }
            } //================================DOI=========================================//
            else if (strpos($key, 'DOI') !== false) {
                $ans['DOI'] = $value;
            } //=============================WOS CATEROFY=========================================//
            else if (strpos($key, 'Web of Science Categories') !== false) {
                $value = trim(end(explode('</span>', @$FR_node->innertext)), '"');
                foreach (explode(';', $value) as $category) {
                    $ans['WOS_category'][] = trim($category, ' ');
                }
            } //=============================RESEARCH AREA=========================================//
            else if (strpos($key, 'Research Areas') !== false) {
                $value = trim(end(explode('</span>', @$FR_node->innertext)), '"');
                foreach (explode(';', $value) as $category) {
                    $ans['research_area'][] = trim($category, ' ');
                }
            } //================================UT=========================================//
            else if (strpos($key, 'Accession Number') !== false) {
                $ans['UT'] = $value;
            } //================================AUTHOR=========================================//
            else if (strpos($key, 'By:') !== false) {
                $ans['author'] = array();
                $text_blocks   = getTextBlocks($FR_node);
                foreach ($FR_node->find('a[title="Find more records by this author"]') as $n => $a) {
                    parse_str(parse_url($a->href, PHP_URL_QUERY), $url_attr);
                    $author = isset($url_attr['author_name']) ? $url_attr['author_name'] : $url_attr['value'];

                    $sup = $a->next_sibling();
                    if ($sup && $sup_b = $sup->find('b a b', 0)) {
                        $addr = $address_list[intval($sup_b->innertext)];
                    } else {
                        if (isset($address_list[$n + 1])) {
                            $addr = $address_list[$n + 1];
                        } else if (isset($address_list[1])) {
                            $addr = $address_list[1];
                        } else {
                            $addr = null;
                        }
                    }

                    $ans['author_detail'][] = array(
                        'name'       => getAuthorFullName($text_blocks, $author),
                        'short_name' => $author,
                        'address'    => $addr,
                    );
                    //author address

                }

            }

        }

        $dom->clear();
        return $ans;
    }
