<?php
    /**
     * Created by PhpStorm.
     * User: ewdchn
     * Date: 3/4/14
     * Time: 2:29 PM
     */
    require_once('Helper.php');

    class Entry implements JsonSerializable
    {

        public static $entry_list = array();

        public static function findEntry($_attr)
        {
            if (!isset(self::$entry_list[$_attr['REFID']])) {
                self::$entry_list[$_attr['REFID']] = new Entry($_attr);
            }
            return self::$entry_list[$_attr['REFID']];
        }

        public $attr = array(
            //
            "UT"             => null,
            "title"          => null,
            "DOI"            => null,
            "abstract"       => null,
            "published"      => null,
            //
            "source"         => null,
            "WOS_category"   => null,
            "research_area"  => null,
            "REFID"          => null,
            //
            "author"         => array(),
            "author_keyword" => array(),
            "keywords_plus"  => array(),
            "citation"       => array(),
            "reference"      => array(),
        );
        public $html = null;


        function __construct($_attr)
        {
            foreach ($this->attr as $key => $value) {
                if (isset($_attr[$key])) {
                    $this->attr[$key] = $_attr[$key];
                }
            }
            if (!$this->integrityCheck()) {
                unset($this);
                throw new \Exception('entry invalid');
            }
        }

        public function jsonSerialize()
        {
            return $this->attr;
        }

        public function integrityCheck()
        {
            if (!$this->attr['title'] || (!$this->attr['UT'] && !$this->attr['REFID'])) {
                return false;
            }
            return true;
        }

        public function variableCheck()
        {
            foreach ($this->attr as $key => $value) {
                if ($value) {
                    return true;
                }
                return false;
            }
        }

        public function setReference($_arr)
        {
            if (!is_array($_arr)) return;
            $this->attr['reference'] = $_arr;
        }

        public function getAttr($key)
        {
            if (isset($this->attr[$key]))
                return $this->attr[$key];
            else return false;
        }
    }