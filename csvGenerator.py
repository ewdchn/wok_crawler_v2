# -*- coding: utf-8 -*-
import sys
import os
import MySQLdb
import MySQLdb.cursors
import csv
import passwd


def generateEntryCsv():
    con = MySQLdb.connect("localhost",passwd.USER,passwd.PASSWD,"WOK_II",cursorclass=MySQLdb.cursors.DictCursor)
    cur = con.cursor()
    print "generate csv"
    cur.execute('DESCRIBE entry;')
    column_key_rows = cur.fetchall()
    with open('entry.csv','wb') as csv_file:
        writer = csv.writer(csv_file,dialect=csv.excel,quoting=csv.QUOTE_ALL)
        entry_attrs = [x['Field'] for x in column_key_rows]
        all_attrs = entry_attrs+['authors','research area','source','author keywords','keywords plus']
        writer.writerow(all_attrs)
        cur.execute('SELECT * FROM {0} e  WHERE e.REFID IN (SELECT DISTINCT REFID_I FROM {1})'.format('entry','entry_coreference'))
        entry_rows = cur.fetchall()
        for row in entry_rows:
            #authors
            v = []
            extra_attrs = ['author','research_area','WOS_category','source','research_area','author_keyword','keywords_plus']
            for key in entry_attrs:
                v.append(row[key])
            for key in extra_attrs:
                cur.execute('SELECT {0} FROM {0} a WHERE a.REFID={1}'.format(key,"'"+row['REFID']+"'"))
                attrs = cur.fetchall()
                v.append(' '.join(['"'+x[key]+'"' for x in attrs]))
            writer.writerow(v)

def generateAuthorKeywords():
    con = MySQLdb.connect("localhost",passwd.USER,passwd.PASSWD,"WOK_II",cursorclass=MySQLdb.cursors.DictCursor)
    cur = con.cursor()
    with open('author_keywords.csv','wb') as csv_file:
        writer = csv.writer(csv_file,dialect=csv.excel,quoting=csv.QUOTE_ALL)
        cur.execute('SELECT DISTINCT a.author_keyword FROM author_keyword a,entry_coreference e WHERE a.REFID=e.REFID_I')
        writer.writerow(["author_keywords"])
        for row in cur.fetchall():
          writer.writerow([row['author_keyword']])


def generateAuthorCsv():
    con = MySQLdb.connect("localhost",passwd.USER,passwd.PASSWD,"WOK_II",cursorclass=MySQLdb.cursors.DictCursor)
    cur = con.cursor()
    cur.execute('DESCRIBE author_detail;')
    column_key_rows = cur.fetchall()
    with open('author.csv','wb') as csv_file:
        writer = csv.writer(csv_file,dialect=csv.excel,quoting=csv.QUOTE_ALL)
        entry_attrs = [x['Field'] for x in column_key_rows]
        writer.writerow(entry_attrs)
        cur.execute('SELECT * from author_detail;')
        for row in cur.fetchall():
            v = []
            for key in entry_attrs:
                v.append(row[key])
            writer.writerow(v)

generateEntryCsv()
