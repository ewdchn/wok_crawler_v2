<?php

    /**
     * Deprecated, nothing to see here
     *
     * Just some random tests
     *
     *
     *
     */
    require_once('main.php');


    function relatedSearchURLGen($_WOS, $_SID)
    {
        $URL = 'apps.webofknowledge.com/InterService.do?product=WOS&toPID=WOS&action=AllCitationService&isLinks=yes&highlighted_tab=WOS&last_prod=WOS&fromPID=UA&srcDesc=RET2UA&srcAlt=Back+to+All+Databases&UT=' . $_WOS . '&parentProduct=UA&search_mode=RelatedRecords&SID=' . $_SID;
        $URL .= '&page=1&action=changePageSize&pageSize=50';
        return $URL;
    }

    function refSearchParseTest()
    {
        for ($i = 1; $i <= 4; $i++) {

            $fileName = 'ref_search_test_0' . $i . '.htm';
            var_dump(\Parser\parseRefSearchPage(file_get_contents('test/' . $fileName)));
        }
    }

    function entryParseTest()
    {
        for ($i = 1; $i <= 4; $i++) {
            //        echo "\n$i\n";
            $fileName = 'entry_test_0' . $i . '.htm';
            //        var_dump(\Parser\parseEntryPage(file_get_contents('test/'.$fileName)));
            \Parser\parseEntryPage(file_get_contents('test/' . $fileName));
        }
        var_dump(\Author::$author_list);

    }

    function authorRefSearchTest()
    {
        Author::constrctFromJson(file_get_contents('authors.json'));
        echo sizeof(Author::$author_list);
        authorRefSearch(\Author::$author_list);
        file_put_contents('authors_with_count.json', json_encode(\Author::$author_list));
        insertDbAuthors(\Author::$author_list);
    }

    function variableCheck($attr)
    {
        foreach ($attr as $key => $value) {
            if ($value) {
                echo "true";
            }
        }
        echo "false";

    }

    function testUTEntry()
    {
        $crawler   = new \Crawler\Crawler();
        $sess_attr = \Parser\parseTopicSearchPage($crawler->topicSearch());
        var_dump(getReference($crawler, $sess_attr['SID'], 'WOS:A1996VU79800012'));
        var_dump(getReference($crawler, $sess_attr['SID'], 'WOS:000333771300009'));
//        foreach(Entry::$entry_list as $v){
//            echo json_encode($v);
//        }
    }

    function testDateParse()
    {
        $entries = json_decode(file_get_contents('entries.json'),true);
        foreach($entries as $e){
            echo $e['published']." : ".parseDate($e['published'])."\n";
        }
    }

    testDateParse();

?>
