<?php
    namespace Crawler;

    require_once('common/Helper.php');
    require_once('form_data.php');

    /**
     * Class Crawler
     * @package Crawler
     * Every instance of a crawler is an isolated session between the site
     */
    class Crawler
    {
        public static $session_cnt = 0;
        public $session_no;
        public $cookie_storage;

        function __construct()
        {
            try {
                checkOrCreateDir("cookies");
                $this->session_no     = self::$session_cnt;
                $this->cookie_storage = "cookies/cookie_" . $this->session_no . ".txt";
                file_put_contents($this->cookie_storage, "");
                self::$session_cnt++;
                $main_page = initSession($this->cookie_storage);

            } catch (Exception $e) {
                print $e->getMessage();
                exit(0);
            }
        }

        /*
         * clear cookies
         */
        function __destruct()
        {
            file_put_contents($this->cookie_storage, "");
        }

        /**
         * @return array|bool
         */
        public function topicSearch()
        {
            $options                     = array();
            $options[CURLOPT_URL]        = 'http://apps.webofknowledge.com/UA_GeneralSearch.do';
            $options[CURLOPT_POST]       = true;
            $options[CURLOPT_POSTFIELDS] = http_build_query($GLOBALS['form_data_arr']);
            try {
                $response = getPage($options, $this->cookie_storage);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return $response;
        }

        /**
         * @param string $_author_ID
         * @return array|bool
         */
        public function authorSearch($_author_ID)
        {
            parse_str('product=UA&search_mode=OneClickSearch&excludeEventConfig=ExcludeIfFromFullRecPage&field=AU&cacheurlFromRightClick=no', $params);
            $params['value']      = $_author_ID;
            $options              = array();
            $options[CURLOPT_URL] = 'http://apps.webofknowledge.com/OneClickSearch.do?' . http_build_query($params);
            try {
                $response = getPage($options, $this->cookie_storage);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return $response;
        }


        /**
         * @param string $_cit_report_lnk
         * @return array|bool
         */
        public function getCitationReport($_cit_report_lnk)
        {
            $options              = array();
            $options[CURLOPT_URL] = $_cit_report_lnk;
            try {
                $response = getPage($options, $this->cookie_storage);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return $response;
        }

        /**
         * @param string $_REFID
         * @return array|bool
         */
        public function citationSearch($_REFID)
        {
            //http://apps.webofknowledge.com/CitingArticles.do?product=UA&search_mode=CitingArticles&parentProduct=UA&REFID=424847&excludeEventConfig=ExcludeIfFromNonInterProduct
            //http://apps.webofknowledge.com/CitingArticles.do?product=WOS&REFID=424847&SID=Y1RnwqHVEFIVKdaD736&search_mode=CitingArticles
            $options              = array();
            $options[CURLOPT_URL] = 'apps.webofknowledge.com/CitingArticles.do?product=WOS&search_mode=CitingArticles&REFID=' . $_REFID;
            try {
                $response = getPage($options, $this->cookie_storage);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return $response;
        }

        /**
         * @param string $_qid
         * @param string $_SID
         * @param string $_doc_id
         * @return string html_response (w/out header)
         */
        public function grabEntryPage($_qid, $_SID, $_doc_id)
        {
            //http://apps.webofknowledge.com/full_record.do?product=UA&qid=55&SID=S1ZfoPzdCKkfTupe6Lt&doc=8
            $options              = array();
            $options[CURLOPT_URL] = 'apps.webofknowledge.com/full_record.do?product=UA&qid=' . $_qid . '&SID=' . $_SID . '&doc=' . $_doc_id;
            try {
                $response      = getPage($options, $this->cookie_storage);
                $html_response = end(seperateHeader($response[0], $response[2]));
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return html_entity_decode($html_response);
        }

        /**
         * @param string $_qid
         * @param string $_SID
         * @param string $_doc_id
         * @return mixed
         */
        public function grabEntryPageRefMode($_qid, $_SID, $_doc_id)
        {
            //http://apps.webofknowledge.com/full_record.do?product=WOS&qid=55&SID=S1ZfoPzdCKkfTupe6Lt&doc=8
            $options              = array();
            $options[CURLOPT_URL] = 'apps.webofknowledge.com/full_record.do?product=WOS&search_mode=CitingArticles&qid=' . $_qid . '&SID=' . $_SID . '&doc=' . $_doc_id;
            try {
                $response      = getPage($options, $this->cookie_storage);
                $html_response = end(seperateHeader($response[0], $response[2]));
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return html_entity_decode($html_response);
        }

        public function refineSearch($_qid, $_SID)
        {
            $post_data                   = array(
                'parentQid' => $_qid,
                'SID'       => $_SID,
            );
            $options                     = array();
            $options[CURLOPT_URL]        = 'http://apps.webofknowledge.com/Refine.do';
            $options[CURLOPT_POST]       = true;
            $options[CURLOPT_POSTFIELDS] = $GLOBALS['refine_data_str'] . http_build_query($post_data);
            try {
                $response = getPage($options, $this->cookie_storage);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return $response;
        }

        public function refineSearchAlt($_qid, $_SID, $_refine_selection)
        {
            $refine_post_str_I  = '&product=UA&databaseId=FASTRDB&search_mode=Refine&queryOption%28summary_search_mode%29=OneClickSearch&action=search&clickRaMore=Your+refine+selection+on+the+screen+will+not+be+remembered+if+you+continue+with+%27more+...%27+feature.&openCheckboxes=Your+refine+selection+in+this+left-panel+will+not+be+remembered+if+you+hide+it.&refineSelectAtLeastOneCheckbox=Please+select+at+least+one+checkbox+to+refine+your+results.&queryOption%28sortBy%29=PY.D%3BLD.D%3BSO.A%3BVL.D%3BPG.A%3BAU.A&queryOption%28ss_query_language%29=auto&sws=&defaultsws=Search+within+results+for...&swsFields=TS&swsHidden=Search+within+the+first+100%2C000%3Cbr%3Eresults+for&exclude=&exclude=&exclude=';
            $refine_post_str_II = '&exclude=&exclude=&exclude=&exclude=&exclude=&exclude=&exclude=&exclude=&exclude=&exclude=&mode=refine';

            $sess_data            = array(
                'parentQid' => $_qid,
                'SID'       => $_SID,
            );
            $post_data            = array();
            $refine_selection_str = '';
            if (!empty($_refine_selection))
                foreach ($_refine_selection as $r) {
                    $refine_selection_str .= '&refineSelection=' . htmlentities($r);
                }
            else
                throw new \Exception('no refine field');

            $options                     = array();
            $options[CURLOPT_URL]        = 'http://apps.webofknowledge.com/Refine.do';
            $options[CURLOPT_POST]       = true;
            $options[CURLOPT_POSTFIELDS] = http_build_query($sess_data) . $refine_post_str_I . $refine_selection_str . $refine_post_str_II;
            echo "\n" . $options[CURLOPT_POSTFIELDS] . "\n";
            try {
                $response = getPage($options, $this->cookie_storage);
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return $response;
        }

        /**
         * @param $_SID
         * @param $_UT
         */
        public function getEntryPageWithUT($_SID, $_UT)
        {
            //http://apps.webofknowledge.com/CitedFullRecord.do?product=WOS&colName=WOS&SID=S28AMTKBiH2IJsE4Bxt&search_mode=CitedFullRecord&isickref=WOS:000251377600005
            $options              = array();
            $options[CURLOPT_URL] = 'http://apps.webofknowledge.com/CitedFullRecord.do?product=WOS&colName=WOS&search_mode=CitedFullRecord&SID=' . $_SID . '&isickref=' . $_UT;
            try {
                $response      = getPage($options, $this->cookie_storage);
                $html_response = end(seperateHeader($response[0], $response[2]));
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return html_entity_decode($html_response);
        }

        /**
         * @param $_UT
         * @return string
         */
        public function refSearch($_UT)
        {
            $options              = array();
            $options[CURLOPT_URL] = 'http://apps.webofknowledge.com/InterService.do?product=WOS&toPID=WOS&action=AllCitationService&isLinks=yes&highlighted_tab=WOS&last_prod=WOS&fromPID=UA&srcDesc=RET2UA&srcAlt=Back+to+All+Databases&UT='.$_UT.'&search_mode=CitedRefList&recid='.$_UT.'&PREC_REFCOUNT=15&fromRightPanel=true';
//            echo "\n".$options[CURLOPT_URL]."\n";
            try {
                $response      = getPage($options, $this->cookie_storage);
                $html_response = end(seperateHeader($response[0], $response[2]));
            } catch (\Exception $e) {
                echo $e->getTraceAsString();
                exit(0);
            }
            return html_entity_decode($html_response);
        }

        /**
         * @param $_url
         * @return string
         */
        public function getPageWithURL($_url){
            $_url=parse_url($_url,PHP_URL_HOST)?$_url:'apps.webofknowledge.com/'.$_url;
            $options              = array();
            $options[CURLOPT_URL] = $_url;
//            echo "\n".$options[CURLOPT_URL]."\n";
            try {
                $response      = getPage($options, $this->cookie_storage);
                $html_response = end(seperateHeader($response[0], $response[2]));
            } catch (\Exception $e) {
                echo "ERROR: ".$_url."\n";
                echo $e->getTraceAsString();
                exit(0);
            }
            return html_entity_decode($html_response);
        }
    }
