<?php
/**
 * Created by PhpStorm.
 * User: ewdchn
 * Date: 3/4/14
 * Time: 2:30 PM
 */


class Author implements JsonSerializable
{
    public static $author_list = array();

    public $name;
    public $short_name;
    public $address;
    public $entry_cnt;
    public $citation_cnt;


    /**
     * @param $_name
     * @param $_short_name
     * @param $_address
     * @return mixed
     */
    public static function getAuthor($_name, $_short_name, $_address)
    {
        $_short_name      = strtolower($_short_name);
        $_name = strtolower($_name);
        if (!isset(self::$author_list[$_name])) {
            self::$author_list[$_name] = new Author($_name, $_short_name, $_address);
        }
        if (!self::$author_list[$_name]->address && $_address) {
            self::$author_list[$_name]->setAddress($_address);
        }
        return self::$author_list[$_name];
    }

    /**
     * @param string $_json
     */
    public static function constrctFromJson($_json)
    {
        self::$author_list=array();
        $authors = (array)json_decode($_json);
        foreach ($authors as $key => $author) {
            $auth = new Author($key,$author->short_name,$author->address);
            $auth->setEntryCnt($author->entry_cnt);
            $auth->setCitationCnt($author->citation_cnt);
            self::$author_list[$key] =$auth;
        }
    }

    /**
     * @param $_name
     * @param $_short_name
     * @param $_address
     *
     */
    function __construct($_name, $_short_name, $_address)
    {
        $this->name       = $_name;
        $this->short_name = $_short_name;
        $this->address    = $_address;
//        var_dump($this);
    }


    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    public function getName()
    {
        return $this->getFullName();
    }

    public function getFullName()
    {
        return $this->name;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


    /**
     * @param mixed $entry_cnt
     */
    public function setEntryCnt($entry_cnt)
    {
        $this->entry_cnt = $entry_cnt;
    }

    /**
     * @param mixed $citation_cnt
     */
    public function setCitationCnt($citation_cnt)
    {
        $this->citation_cnt = $citation_cnt;
    }
} 
