# README #

### Executing ###
* Use main.php to instantiate crawler to grab data and store the result in json
* Run op_*.php to generate graphs from results from crawler


### Components ###
* op_*
    * Different operation to generate graphs
* Main
    * Main crawler body, defines how data is grabbed and store to json
* Crawler
    * Grab data from website, manage sessions
* Parser  
    * Parse the data from crawler  
* Entry
    * Class Representing the data of an entry in Web of Science
* Author
    * Class Representing the data of an author in Web of Science

### Dependency ###
*  simple html dom for html parsing